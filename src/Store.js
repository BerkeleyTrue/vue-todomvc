import R from 'ramda';
import Vue from 'vue';
import { Store } from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import vueHistory from './history-plugin.js';

const routes = {
  '/': 'all',
  '/active': 'active',
  '/completed': 'completed',
};

export default () => new Store({
  plugins: [createPersistedState(), vueHistory(routes)],
  state: {
    todos: [],
    todosById: {},
    currentId: 0,
    nowShowing: 'all',
  },
  getters: {
    allToggled: R.pipe(
      R.prop('todosById'),
      R.values,
      R.pluck('completed'),
      R.all(R.identity),
    ),
    todosShowing: (state) => R.pipe(
      R.prop('todos'),
      R.map(id => state.todosById[id]),
      R.filter(({ completed }) => (
        state.nowShowing === 'all' ||
        (state.nowShowing === 'completed' && completed) ||
        (state.nowShowing === 'active' && !completed)
      )),
      R.pluck('id'),
    )(state),
    getTodoById: state => id => state.todosById[id],
    completedCount: R.pipe(
      R.prop('todosById'),
      R.values,
      R.pluck('completed'),
      R.reduce((count, completed) => (completed ? count + 1 : count), 0)
    ),
    show: R.pipe(R.prop('todos'), R.length, Boolean),
    count: R.pipe(R.prop('todos'), R.length),
  },
  mutations: {
    appMounted() {},
    appDestroyed() {},
    submitTodo(state, title) {
      const { todos, todosById } = state;
      const todo = {
        id: state.currentId++,
        title,
        completed: false,
        editing: false,
      };
      todos.push(todo.id);
      Vue.set(todosById, todo.id, todo);
    },
    toggleAll({ todosById }, checked) {
      R.mapObjIndexed(todo => {
        todo.completed = checked;
        return todo;
      })(todosById);
    },
    toggleCompleted({ todosById }, id) {
      const todo = todosById[id];
      if (todo && todo.title) {
        todo.completed = !todo.completed;
      }
    },
    updateTodoTitle({ todosById }, { id, title }) {
      const todo = todosById[id];
      todo.title = title;
    },
    clearCompleted(state) {
      const todosToRemove = R.pipe(
        R.values,
        R.filter(R.prop('completed')),
        R.pluck('id'),
      )(state.todosById);
      state.todosById = R.omit(todosToRemove, state.todosById);
      state.todos = R.without(todosToRemove, state.todos);
    },
    destroyTodo(state, id) {
      delete state.todosById[id];
      state.todos = R.reject(R.equals(id), state.todos);
    },
    updateRoute(state, nowShowing = 'all') {
      state.nowShowing = nowShowing;
    },
    updateFromStorage(state, updates) {
      if (updates && updates.todos) {
        updates.todos = R.uniq(updates.todos);
        Object.assign(state, updates);
      }
    }
  },
});
