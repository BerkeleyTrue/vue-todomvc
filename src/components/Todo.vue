<template lang='pug'>
  li(:class='classnames')
    .view
      input.toggle(
        :checked='todo.completed'
        @change='onChange'
        type='checkbox'
      )
      label(@dblclick='handleDoubeClick') {{ todo.title }}
      button.destroy(@click='destroyThisTodo')
    input.edit(
      @blur='handleSubmit'
      @keydown='handleKeyDown'
      ref='edit'
      type='text'
      v-model.trim='editText'
    )
</template>

<script>
  import { mapMutations } from 'vuex';

  export default {
    name: 'Item',
    props: {
      id: {
        type: Number,
        required: true,
      },
    },
    data: function() {
      return {
        editing: false,
        editText: '',
      };
    },
    computed: {
      todo: vm => {
        const todo = vm.$store.getters.getTodoById(vm.id);
        vm.editText = todo.title;
        return todo;
      },
      classnames() {
        return {
          completed: this.todo.completed,
          editing: this.editing
        };
      },
    },
    methods: {
      ...mapMutations([
        'updateTodoTitle',
        'destroyTodo',
        'toggleCompleted',
      ]),
      handleKeyDown: function({ key }) {
        if (key === 'Enter') {
          this.handleSubmit();
        }
      },
      handleDoubeClick: function() {
        this.editing = true;
        this.$nextTick(() => {
          this.$refs.edit.focus();
        });
      },
      handleSubmit: function() {
        this.editing = false;
        this.updateTodoTitle({ id: this.id, title: this.editText });
      },
      destroyThisTodo: function() {
        this.destroyTodo(this.id);
      },
      onChange: function() {
        this.toggleCompleted(this.id);
      },
    }
  }
</script>
