import createHistory from 'history/createHashHistory';

export default routes => store => {
  let history;
  let unlisten;
  store.subscribe(({ type }) => {
    if (!history && type === 'appMounted') {
      history = createHistory();
      unlisten = history.listen(({ pathname }) => {
        const route = routes[pathname] || routes['/'];
        store.commit('updateRoute', route);
      });
    }
    if (unlisten && type === 'appDestroyed') {
      unlisten();
      unlisten = null;
      history = null;
    }
  });
};
