import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
import createStore from './Store.js';

Vue.config.productionTip = false
Vue.use(Vuex);

new Vue({
  mounted() { this.$store.commit('appMounted'); },
  destroyed() { this.$store.commit('appDestroyed'); },
  store: createStore(),
  render: h => h(App),
}).$mount('#app')
